// Kripta.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#define _CRT_SECURE_NO_WARNINGS
#define WIN32_LEAN_ANDMEAN
#include <Windows.h>
#include <Wincrypt.h>
#include <stdio.h>
#include <iostream>
#include <conio.h>
#include <Error.h>
#include <ctime>

#pragma comment(lib, "advapi32.lib")

DWORD       cbName;
DWORD       dwType;
DWORD       dwIndex;
LPTSTR      Name;
HCRYPTPROV hProv;
HCRYPTKEY hSessionKey;

void EncryptSpeedTest()
{
    // ��������� ��������� ����������������
    if (!CryptAcquireContext(&hProv, NULL, NULL, 
        PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
    {
        std::cout << "Context Error" << std::endl;
        return;
    }

    std::cout << "Cryptographic provider initialized" << std::endl;

     // ��������� ����������� �����
    if (!CryptGenKey(hProv, CALG_RC4, CRYPT_EXPORTABLE, &hSessionKey))
    {
        std::cout << "Key Gen Error" << std::endl;
        return;
    }

    std::cout << "Session key generated" << std::endl;


    unsigned int startTime = clock();
    const unsigned int str_size = 1000000;
    char message[str_size];
    for (int i = 0; i < str_size -1; i++) {
        message[i] = 'q';
    }
    message[str_size -1] = '\0';
    DWORD count = strlen(message);
    // ���������� ������
    if (!CryptEncrypt(hSessionKey, 0, true, 0, (BYTE*)message,
        &count, strlen(message)))
    {
        std::cout << "Encrypt Error" << std::endl;
    }
    std::cout << "Encryption completed" << std::endl;

    /*CryptDecrypt(hSessionKey, 0, true, 0, (BYTE*)message,
        &count);
    
    std::cout << message;*/
    

    unsigned int endTime = clock();
    
    //����� ���������� ����� �� �����
    std::cout << str_size / (double)(endTime - startTime) * 1000 / 1000000 << " Mb per second" << std::endl;
}

void printAllCryptoProviders()
{
	
	printf("Listing Available Providers:\n");
    printf("Provider type\tProvider Name\n");
    printf("__\t__"
        "___\n");

    // ��������� ���� ������������ �����������
    dwIndex = 0;
    while (CryptEnumProviders(
        dwIndex,
        NULL,
        0,
        &dwType,
        NULL,
        &cbName
    ))
    {

        //-----------------------------------------------------------
        //  cbName ���������� ����� ����� ���������� ����������. �������� ������ � ������ ��� ���������� ����� �����.

        if (!(Name = (LPTSTR)LocalAlloc(LMEM_ZEROINIT, cbName)))
        {
            printf("ERROR - LocalAlloc failed\n");
            exit(1);
        }
        //-----------------------------------------------------------
        //  �������� ��� ����������.
        if (CryptEnumProviders(
            dwIndex++,
            NULL,
            0,
            &dwType,
            Name,
            &cbName
        ))
        {
            printf("     %4.0d\t%S\n", dwType, Name);
        }
        else
        {
            printf("ERROR - CryptEnumProviders failed.\n");
            exit(1);
        }
        LocalFree(Name);

    } 
}


void main()
{
    // ���������� � ������������� ����������.
    

    // ����
    // ������� 1
    printf("task 1 \n");
    // ������ ����� ��������� ��� �����������.
	printAllCryptoProviders();

    EncryptSpeedTest();
    
	getch();
}